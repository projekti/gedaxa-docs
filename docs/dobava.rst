======
DOBAVA
======

V dobavi najdemo vse kar je povezano z naročili dobave in dobavnicami. Vsa naročila in dobavnice so vezane na
dobavitelja, ter jih lahko začnemo ustvarjati šele ko imamo te definirane (gledamo sekcijo *Kako ustvarimo partnerja*)
Ko je naročilo ustvarjeno, se postavke na naročilu dodajo v skladišče pričakovanega materiala, ob zapiranju dobavnice pa
postavke in njihovo količino razporedimo v posamezna skladišča.

**************
SEZNAM NAROČIL
**************

V seznamu naročil vidimo vsa naročila, ki so bila ustvarjena v sistemu. Naročila imajo različna stanja (*Odprto*,
*Potrjeno* in *Preklicano*). Ko ustvarimo naročilo dobave ta dobi stanje *Odprto* in jo lahko urejamo vse dokler je ne
*Potridimo* oz. *Prekličemo*. Ko naročilo potrdimo se postavke in njihova količina zabeležita v *Skladišče pričakovanega
materiala* (gledamo sekcijo *Kaj je skladišče pričakovanega materiala*), ter ostane tam dokler ne zaključimo dobavnice,
ki se nanaša na to *Naročilo*

.. image:: https://gitlab.com/projekti/gedaxa-docs/raw/master/imgs/procurement.png


Kako ustvarimo naročilo dobave
++++++++++++++++++++++++++++++

V *Seznamu naročil*, kliknemo na gumb *Dodaj naročilnico*.

.. image:: https://gitlab.com/projekti/gedaxa-docs/raw/master/imgs/procurement_add_1.png

Odpre se novo okno *Ustvari naročilnico*, kjer izpolnimo
manjkajoče podatke. Če dobavitelja ni na seznamu, ga moramo vnesti kot *Partnerja*, ali pa obstoječega partnerja
označiti kot dobavitelja (gledamo *Kako dodam novega partnerja, dobavitelja in/ali naročnika*).
Po kliku *Shrani*, se naročilo shrani, nato pa lahko začnemo dodajati postavke.

.. image:: https://gitlab.com/projekti/gedaxa-docs/raw/master/imgs/procurement_add_2.png

Kako dodamo postavke in storitve na naročilo dobave
+++++++++++++++++++++++++++++++++++++++++++++++++++

V Seznamu naročil (*Dobava* > *Seznam naročil*), kliknemo na ikono za urejanje |pencil| v stolpcu *Ukazi*.

.. image:: https://gitlab.com/projekti/gedaxa-docs/raw/master/imgs/procurement_add_items_1.png

Odpre se okno *Posodobitve naročilnice*, kjer izberemo zavihek *Postavke naročila* (poleg zavihka *Glavno*), nato
pa pritisnemo gumb *Dodaj postavko*.

.. image:: https://gitlab.com/projekti/gedaxa-docs/raw/master/imgs/procurement_add_items_2.png

Odpre se obrazec za vnos podatkov, kjer iz padajočega menija izberemo postavko (če je ni, jo dodamo med šifrante,
glej *Kako dodamo nov šifrant*), količino postavke v izbrani merski enoti, cena na enoto in rok dostave postavke.

Pravilno shranjena postavka se prikaže v tabeli *Postavke naročila* na isti strani. Če želimo, jo lahko izbrišemo
ali uredimo s klikom na ikono v okolju *Ukazi*.

.. image:: https://gitlab.com/projekti/gedaxa-docs/raw/master/imgs/procurement_edit_items.png

Kako uredimo, izbrišemo, potrdimo, prekličemo naročilo ali nanj dodamo datoteke
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

V *Seznamu naročil* (*Dobava* > *Seznam naročil*) v stolpcu *Ukazi* najdemo v kolikor je naročilo odprto 5 ikon,
durgače 3 ikone, s katerimi lahko naročilo dobave uredimo, izpišemo, nanj dodamo datoteke, potrdimo ali ga prekličemo
ter izbrišemo. Urejanje, potrjevanje, preklicevanje naročila je možno v kolikor je njegovo stanje odprto.
Naročilo pa lahko izpišemo po tem ko smo ga potrdili.

.. image:: https://gitlab.com/projekti/gedaxa-docs/raw/master/imgs/procurement_confirm.png

Zakaj ne morem dodati postavke na naročilo?
+++++++++++++++++++++++++++++++++++++++++++

Najverjetneje, ker je bilo naročilo potrjeno ali preklicano. V tem primeru moramo narediti novo naročilo dobave,
kamor dodajamo postavke.

*********
DOBAVNICE
*********

Dobavnice so vezane na naročila dobave. Ko je naročilo potrjeno se postavke in njihova količina zabeležijo v *Skladišču
pričakovanega materiala* (gledamo sekcijo *Kaj je skladišče pričakovanega materiala*), tam pa ostanejo dokler z
dobavnicami ne zaključimo celotne količine pričakovanega materiala (eno naročilo lahko zaključujemo z večimi
dobavnicami).

Kako ustvarim dobavnico
+++++++++++++++++++++++

V pogledu *Dobavnice* (*Dobava* > *Dobavnice*) kliknemo na gumb *Ustvari dobavnico*, ki odpre obrazec za vnos podatkov.
Šifro izberemo poljubno, dobavitelja pa izberemo iz padajočega menija. Če ne najdemo dobavitelja med možnostmi, moramo
dobavitelja vnesti (gledamo sekcijo *Kako dodam novega partnerja, dobavitelja in/ali naročnika*).

.. image:: https://gitlab.com/projekti/gedaxa-docs/raw/master/imgs/procurement_delivery_notes.png

Ko imamo dobavnico ustvarjeno, lahko začnemo z zaključevanjem dobavljenih artiklov na posamezna naročila, ki so
bila poslana dobavitelju, katerega smo izbrali v prejšnem koraku. V pogledu *Seznam
dobavnic* v polju *Ukazi* kliknemo gumb za urejanje |pencil| (urejamo lahko vse dobavnice, ki še niso zaključene).
Ta nas pelje v novo okno, kjer so vidna vsa odprta naročila (naročila, ki še nimajo zaključene vseh količin postavk).

.. image:: https://gitlab.com/projekti/gedaxa-docs/raw/master/imgs/procurement_delivery_notes_edit.png

Dobavnica je razdeljena na vsa nezaključena naročila (naročila katere količine postavk še ni bila v celoti zaprta), ki
so bila poslana dobaviteljem. Za posamezno naročilo lahko zaključujemo vse postavke, vendar samo do količine, ki smo jo
definirali na samem naročilu. V kolikor je bil del postavke naročila zaključen že z drugo dobavnico (naročilo lahko
zaključujemo z večimi dobavnicami) nam sistem dovoli zaključiti le še preostanek količine, ki jo moramo še zaključiti.

.. image:: https://gitlab.com/projekti/gedaxa-docs/raw/master/imgs/procurement_delivery_notes_orders.png

Za posamezno postavko sistem prikazuje količino, ki smo jo definirali na naročilnico, količino koliko je bilo že zaprto
ter količino koliko moramo še zapreti. S pritiskom na gumb > se nam odpre obrazec za zaključevanje postavk naročila.
Izbrati moramo skladišče v katerega bomo posamezne postavke prenesli, sistem pa ob zaključevanju dobavnice samodejno
popravi zalogo teh artiklov.

.. image:: https://gitlab.com/projekti/gedaxa-docs/raw/master/imgs/procurement_delivery_notes_to_close.png

Na posamezno dobavnico lahko dodajamo storitve, ki smo jih definirali v šifrantih (gledamo sekcijo *Kako dodajamo
šifrant*). Poleg tega lahko dobavnico poljubno shranjujemo, ter jo urejamo dokler je ne zaključimo.

Ko dobavnico v celoti izpolnemo jo lahko zaključimo s pritiskom na gumb *Zaključi*. Sistem samodejno doda artikle v
izbrana skladišča.

.. image:: https://gitlab.com/projekti/gedaxa-docs/raw/master/imgs/procurement_delivery_notes_finish.png

Ko je dobavnica zaključena jo lahko v pogledu *Seznam dobavnic* tudi izpišemo.

.. image:: https://gitlab.com/projekti/gedaxa-docs/raw/master/imgs/procurement_delivery_notes_print.png

Kako dodamo datoteko na dobavnico
+++++++++++++++++++++++++++++++++

V podmeniju *Vse dobavnice* (Dobava > Dobavnice), kliknemo na ikono za datoteke v vrstici dobavnice, na katero
želimo dodati datoteke. Odpre se novo okno Datoteke dobavnice.

.. image:: https://gitlab.com/projekti/gedaxa-docs/raw/master/imgs/procurement_delivery_notes_add_file_1.png


Datoteko dodamo v dveh korakih. Najprej kliknemo gumb *Dodaj datoteko*, nato pa v obrazcu za vnos naložimo dadoteko
iz računalnika, izberemo opis datoteke in stisnemo gumb *Shrani*.

.. image:: https://gitlab.com/projekti/gedaxa-docs/raw/master/imgs/procurement_delivery_notes_add_file_2.png


.. |thick| raw:: html

    <i class="icon-check"></i>

.. |pencil| raw:: html

    <i class="icon-pencil2"></i>

.. |delete| raw:: html

    <i class="icon-close"></i>

.. |object| raw:: html

    <i class="icon-file-import"></i>

.. |icon_file| raw:: html

    <i class="icon-file"></i>

.. |icon_bill| raw:: html

   <i class="icon-bill-2"></i>

.. |icon_edit| raw:: html

   <i class="icon-edit-1"></i>

.. |icon_download| raw:: html

   <i class="icon-download-2"></i>

.. |icon_check_circle| raw:: html

   <i class="icon-check-circle"></i>

.. |icon_cancel_circle| raw:: html

   <i class="icon-cancel-circle"></i>

.. |icon_close| raw:: html

   <i class="icon-close"></i>

.. |icon_trash| raw:: html

   <i class="icon-trash"></i>

.. |icon_arrow_right| raw:: html

   <i class="icon-arrow-2-right"></i>

.. |icon_files| raw:: html

   <i class="icon-files"></i>

.. |icon_updown| raw:: html

   <i class="icon-swap-vertical"></i>
