=======
PROCESI
=======

Ta del dokumentacije se nanaša na dodajanje in urejanje procesov.
Dokumentacija razloži kaj so procesi in kako uporabljamo grafični vmesnik.
Dokumentacija vsebuje tudi razložene pogoste napake, ki jih uporabnik lahko naredi pri uporabi grafičnega vmesnika za
dodajanje in urejanje procesov.

***************
Kaj so procesi?
***************

S pomočjo procesov lahko za izdelke in polizdelke definiramo njihov proces izdelave.
Pri izdelavi procesov nam je v pomoč grafični vmesnik, s pomočjo katerega na enostaven način v proces dodajamo materiale,
izdelke, polizdelke in korake/akcije procesa.
Te gradnike nato med seboj povezujemo in na ta način definiramo postopek za izdelavo izbranega izdelka ali polizdelka.

**************************************
Kako dodam nov izdelek ali polizdelek?
**************************************

Postopek dodajanja novega izdelka je identičen dodajanju novega šifranta.
Za več informacij glej dokumentacijo *Kako dodam nov šifrant?* Ko je izdelek ali polizdelek uspešno dodan v šifrant,
lahko nadaljujete z definiranjem procesa izdelave tega izdelka.
Več o tem kako ustvarimo proces glej dokumentacijo *Kako ustvarim nov proces?*.

*****************************
Kako ustvarim novo operacijo?
*****************************

V glavnem meniju izberete **Operacije** (*Proizvodnja > Operacije*). Kliknite na **Dodaj operacijo**.
Pokaže se vam forma za vnos imena operacije.
Vnos zaključite s klikom na gumb **Potrdi**.

.. image:: https://gitlab.com/projekti/gedaxa-docs/raw/master/imgs/procesi/menu.png
.. image:: https://gitlab.com/projekti/gedaxa-docs/raw/master/imgs/procesi/operacija_dodaj.png

*************************
Kako ustvarim nov proces?
*************************

**Pogoj**: Če želite ustvariti nov proces za željen izdelek, je potrebno da se ta izdelek že nahaja v šifrantu.
Prav tako morate imeti že ustvarjene vse operacije.
Ko je izdelek dodan v šifrant, v glavnem meniju izberete **Procesi** (*Proizvodnja > Procesi*).
Nadaljujete s klikom na gumb *Dodaj proces*.

Vrednosti v stolpcu *Veljavnost* nam povedo ali je proces pravilno sestavljen.

.. image:: https://gitlab.com/projekti/gedaxa-docs/raw/master/imgs/procesi/list.png

Odpre se prvi korak pri dodajanju novega procesa, v katerem je potrebno določiti *Ime* procesa in pa izbrati kateri
izdelek ali polizdelek je končni *Produkt* tega procesa.

.. image:: https://gitlab.com/projekti/gedaxa-docs/raw/master/imgs/procesi/proces_korak_1.png

Nato s klikom na gumb **Potrdi** nadaljujemo na drugi korak izdelave procesa, kjer s pomočjo grafičnega vmesnika
dodajamo elemente procesa in jih med seboj povezujemo.
Na začetku ima proces zgolj en sam gradnik (krog), ki predstavlja *Končni izdelek*.
Cilj izdelave procesa je ustvariti gradnike in jih povezati v *Končni izdelek*.

To naredimo z vstavljanjem procesnih korakov od zadnjega proti prvemu.

Da ustvarimo nov korak kliknemo na gumb *Dodaj korak*.
Takoj se prikaže nov gradnik (krog) na grafu in pod grafom opozorilo *Vsa vozlišča morajo biti povezana*, ki nam pove,
da moramo povezati nov gradnik s končnim izdelkom.

.. image:: https://gitlab.com/projekti/gedaxa-docs/raw/master/imgs/procesi/proces_korak_2.png

Povezavo med gradniki naredimo z naslednjimi koraki.
Kliknemo na gradnik.
S klikom se nam odpre okno okoli njega z nastavitvami gradnika.
Kliknemo na ikono s puščico (|icon_arrow_right|) in nato na naslednji gradnik v procesu (v tem primeru končni izdelek).
Nastane povezava med njima v smeri od novega gradnika k končnemu produktu.
Gedaxa nam takoj pove, katere dve informaciji moramo dodati: izhodni izdelek in operacijo novega koraka.

.. image:: https://gitlab.com/projekti/gedaxa-docs/raw/master/imgs/procesi/proces_korak_3.png
.. image:: https://gitlab.com/projekti/gedaxa-docs/raw/master/imgs/procesi/proces_korak_4.png

Na voljo imamo 5 možnosti:

.. image:: https://gitlab.com/projekti/gedaxa-docs/raw/master/imgs/procesi/node_menu.png

1. gradnik izbrišemo (|icon_trash|),
2. povežemo z drugim gradnikom (|icon_arrow_right|),

3. uredimo in mu dodamo informacije (|pencil|),

.. image:: https://gitlab.com/projekti/gedaxa-docs/raw/master/imgs/procesi/proces_node_edit.PNG

4. preverimo cikle.

.. image:: https://gitlab.com/projekti/gedaxa-docs/raw/master/imgs/procesi/cikli.png

**Menijski dialog zapustimo s klikom na gumb z ikono** |icon_close|.

Postopoma dodajamo korake procesa, jih definiramo glede na tehnične operacije in vhodni ter izhodni material, da dobimo graf celotnega procesa.

Oboje definiramo za nov korak, tako da ponovno kliknemo nanj, da se odpre njegov meni in izberemo ikono za urejanje (|pencil|).
Odpre se forma za urejanje koraka. Dodamo ji informativno ime in opis, izberemo pod *Ukazi* eno izmed operacij,
ki jih imamo v sistemu (če je še nimamo, glej *Kako dodam novo operacijo?*) ter izhodni šifrant,
ki je v tem primeru enak kot končni izdelek.

***************************************************************
Zakaj med dvema gradnikoma procesa ne morem ustvariti povezave?
***************************************************************

Pri dodajanju povezav med gradniki procesa obstajao določena pravila:
Vsak gradnik ima lahko samo eno izhodno povezavo, z izjemo kočnega izdelka, ki izhodnih povezav ne sme imeti.
Med dvema gradnikoma lahko obstaja povezava samo v eni smeri. Dvosmerne povezave niso dovoljene.

************************************************
Kako odstranim povezave ali gradnike v procesih?
************************************************

Kliknemo na povezavo v procesnem grafu, ki jo želimo izbrisati, ter v prikazanem oknu z kliknemo na gumb **Izbriši**.

.. image:: https://gitlab.com/projekti/gedaxa-docs/raw/master/imgs/procesi/node_delete.png

.. |thick| raw:: html

    <i class="icon-check"></i>

.. |pencil| raw:: html

    <i class="icon-pencil2"></i>

.. |delete| raw:: html

    <i class="icon-close"></i>

.. |object| raw:: html

    <i class="icon-file-import"></i>

.. |icon_file| raw:: html

    <i class="icon-file"></i>

.. |icon_bill| raw:: html

   <i class="icon-bill-2"></i>

.. |icon_edit| raw:: html

   <i class="icon-edit-1"></i>

.. |icon_download| raw:: html

   <i class="icon-download-2"></i>

.. |icon_check_circle| raw:: html

   <i class="icon-check-circle"></i>

.. |icon_cancel_circle| raw:: html

   <i class="icon-cancel-circle"></i>

.. |icon_close| raw:: html

   <i class="icon-close"></i>

.. |icon_trash| raw:: html

   <i class="icon-trash"></i>

.. |icon_arrow_right| raw:: html

   <i class="icon-arrow-2-right"></i>
