=========
ZAPOSLENI
=========

Ta del dokumentacije se nanaša na sklop funkcionalnosti, ki jih najdete v podmeniju Zaposleni, ki se nahaja v modulu
Kadrovanje.

*****************
Seznam zaposlenih
*****************

Kako dodam novega zaposlenega?
++++++++++++++++++++++++++++++

Izberemo podmeni Zaposleni ki se nahaja v modulu Kadrovanje in nato izberemo opcijo Seznam zaposlenih.
Pokaže se seznam vseh zaposlenih. S klikom na gumb Dodaj uporabnika se nam bo odprla forma za vnos podatkov.
Polja Uporabniško ime, E-mail, Delovno mesto in Geslo so obvezna. Delovna mesta so vnaprej določena,
če bi želeli drugo delovno mesto, nas prosim kontaktirajte. Vnos uporabnika zaključimo s klikom na gumb Potrdi.
Podatki o novem uporabniku se zapišejo v Seznam uporabnikov.

.. image:: https://gitlab.com/projekti/gedaxa-docs/raw/master/imgs/dodaj-zaposlenega.png


Kako določimo za koliko ur je oseba zaposlena?
++++++++++++++++++++++++++++++++++++++++++++++

Izberemo podmeni Zaposleni ki se nahaja v modulu Kadrovanje in nato izberemo opcijo Seznam zaposlenih.
Pokaže se seznam vseh zaposlenih. S klikom na opcijo Nastavitev tedenskih ur, za vsakega uporabnika ločeno,
se nam bo odprla forma za vnos podatkov. Vnos števila ur na teden zaključimo s klikom na gumb Potrdi.
Novi podatki o uporabniku so vidni v Seznamu uporabnikov.

.. image:: https://gitlab.com/projekti/gedaxa-docs/raw/master/imgs/zaposleni-tedenske-ure.png

Za polno zaposlitev uporabnika določimo 40 ur na teden.


Kako uredimo podatke o zaposlenih?
++++++++++++++++++++++++++++++++++

Izberemo podmeni Zaposleni ki se nahaja v modulu Kadrovanje in nato izberemo opcijo Seznam zaposlenih.
Pokaže se seznam vseh zaposlenih. S klikom na opcijo Uredi, za vsakega uporabnika ločeno, se nam bo odprla forma s
podatki uporabnika.

.. image:: https://gitlab.com/projekti/gedaxa-docs/raw/master/imgs/zaposleni-urejanje.png

Podatke lahko spremenimo in spremembe shranimo s klikom na gumb Potrdi.

.. image:: https://gitlab.com/projekti/gedaxa-docs/raw/master/imgs/zaposleni-urejanje-potrdi.png


Kako odstranimo zaposlenega iz Seznama uporabnikov?
+++++++++++++++++++++++++++++++++++++++++++++++++++

Izberemo podmeni Zaposleni ki se nahaja v modulu Kadrovanje in nato izberemo opcijo Seznam zaposlenih.
Pokaže se nam seznam vseh zaposlenih. Pri vsakem uporabniku v tabeli se v stolpcu Ukazi nahaja ikona Izbriši.
S klikom na ikono, vas sistem vpraša, če ste prepričani, da želite odstraniti uporabnika. Izberite Potrdi.
S tem smo deaktivirali uporabnika. Ta se ne more več prijaviti v ERP s svojim uporabniškim imenom in geslom.

.. image:: https://gitlab.com/projekti/gedaxa-docs/raw/master/imgs/zaposleni-izbrisi.png


Kje vidimo statistiko zaposlenih?
+++++++++++++++++++++++++++++++++

Izberemo podmeni Zaposleni ki se nahaja v modulu Kadrovanje in nato izberemo opcijo Seznam zaposlenih.
Pokaže se nam seznam vseh zaposlenih. Pri vsakem uporabniku v tabeli je v stolpcu Ukazi ikona Statistika.
S klikom na to ikono, se nam odpre statistika uporabnika.


******************
Skupine in pravice
******************

Kaj so skupine?
+++++++++++++++

Skupine so množice uporabnikov, ki uporabljajo Gedaxo na podoben način. Na primer delavec v proizvodnji bo v veliki
 meri pregledoval predvsem svoje delovne naloge in opravila za tekoči dan, skladiščnik bo pregledoval zaloge po
 skladiščih in pripravljal material glede na nivojsko sestavnico naročila, vodstvo pa bo pregledovalo metrike
 uspešnosti podjetja. Vsaki skupini lahko dodelimo zgolj določene pravice vpogledov in/ali urejanja informacij v ERP-ju.
 Pravice dodelimo glede na interne odloke o tem, kdo sme dostopati do informacij, in glede na to,
 katere informacije potrebuje zaposleni za opravljanje svojega dela.


Kako ustvarimo novo skupino?
++++++++++++++++++++++++++++

Izberemo podmeni Zaposleni ki se nahaja v modulu Kadrovanje in nato izberemo opcijo Skupine in pravice.
Pokaže se tabela s seznamom vseh skupin. S klikom na gumb Dodaj skupino se nam bo odprla forma za vnos podatkov.
V formo vnesete samo ime skupine.
Vnos skupine zaključite s klikom na gumb Potrdi.

.. image:: https://gitlab.com/projekti/gedaxa-docs/raw/master/imgs/ustvari-skupino.png


Kako dodamo ali odstranimo uporabnika iz skupine?
+++++++++++++++++++++++++++++++++++++++++++++++++

Izberemo podmeni Zaposleni ki se nahaja v modulu Kadrovanje in nato izberemo opcijo Skupine in pravice.
Pokaže se tabela s seznamom vseh skupin. Pri vsaki skupini v tabeli je v stolpcu Ukazi ikona Uredi člane.
Po kliku na to ikono se nam pokaže forma za urejanje članov skupine. Skupini dodamo uporabnika tako, da kliknemo na
ikono + pri željenem uporabniku. Ta se po kliku premakne v levi stolpec med dodane uporabnike tej skupini.
Skupini odstranimo uporabnika tako, da kliknemo na ikono X pri željenem uporabniku.
Ta se po kliku premakne v desni stolpec med ostale uporabnike, ki ne pripadajo tej skupini.

.. image:: https://gitlab.com/projekti/gedaxa-docs/raw/master/imgs/skupina-urejaj-uporabnike.png


Kako definiramo pravice skupine?
++++++++++++++++++++++++++++++++

Izberemo podmeni Zaposleni ki se nahaja v modulu Kadrovanje in nato izberemo opcijo Skupine in pravice.
Pokaže se tabela s seznamom vseh skupin. Pri vsaki skupini v tabeli je v stolpcu Ukazi ikona Uredi pravice.
Po kliku na to ikono se nam pokaže forma za urejanje pravic skupine.
Skupini dodamo pravico tako, da kliknemo na ikono + pri posamezni pravici. Ta se po kliku premakne v levi stolpec
med dodane pravice tej skupini. Skupini odstranimo pravice tako, da kliknemo na ikono X pri posamezni skupini.
Ta se po kliku premakne v desni stolpec med ostale pravice, ki jih ta skupina nima.

.. image:: https://gitlab.com/projekti/gedaxa-docs/raw/master/imgs/skupina-urejaj-pravice.png


***********************
Planirana usposabljanja
***********************

Kako ustvarimo plan usposabljanja?
++++++++++++++++++++++++++++++++++

Izberemo podmeni Zaposleni ki se nahaja v modulu Kadrovanje in nato izberemo opcijo Planirana usposabljanja.
Tu lahko ustvarimo plane o usposabljanju zaposlenih, določimo za katero kvalifikacijo se zaposleni usposablja in
kako pogosto se usposabljanje ponavlja.
S klikom na gumb Dodaj usposabljanje se nam odpre forma za vnos podatkov o usposabljanju.
Vrsta Kvalifikacije, izbira Zaposlenega in Datum usposabljanja so obvezna polja. Če usposabljanje, ki ga ustvarjamo,
še nima vnesene kvalifikacije v sistem, lahko v formi kliknemo ikono + v oknu kvalifikacije in jo vpišemo.
V oknu Opis lahko dodamo poljubne informacije, ki bi bile koristne, npr. kontakt osebe, ki izvaja usposabljanje.

.. image:: https://gitlab.com/projekti/gedaxa-docs/raw/master/imgs/plan-usposabljanja.png


Kako spremenimo oziroma osvežimo plane usposabljanja?
+++++++++++++++++++++++++++++++++++++++++++++++++++++

Če želimo urediti podatke za obstoječe usposabljanje, kliknemo na ikono Uredi. S klikom na ikono Označi kot končano
potrdimo, da je zaposleni opravil usposabljanje in sistem avtomatično določi nov datum usposabljanja glede na interval
ponovitve, ki smo ga določili.


Kje vidimo, kdo je potrdil ali spremenil plane usposabljanja?
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

Izberemo podmeni Zaposleni ki se nahaja v modulu Kadrovanje in nato izberemo opcijo Planirana usposabljanja.
Kliknemo na gumb Dnevnik plana usposabljanja.

.. image:: https://gitlab.com/projekti/gedaxa-docs/raw/master/imgs/plan-usposabljanja-dnevnik-klik.png

Odpre se nam stran, ki prikazuje, kdo je potrdil zaključek usposabljanja (Uporabnik) in kdaj je odobril zaključek
usposabljanja, oziroma označil, da je bilo usposabljanje opravljeno (Datum zaključka).

.. image:: https://gitlab.com/projekti/gedaxa-docs/raw/master/imgs/usposabljanja-dnevnik.png

S klikom na konkretni primer usposabljanja znotraj stolpca Opis se odpre nov pogled Podrobnosti plana usposabljanja,
ki prikaže vse spremembe v usposabljanju za izbrano kvalifikacijo in danega zaposlenega.

.. image:: https://gitlab.com/projekti/gedaxa-docs/raw/master/imgs/usposabljanje-podrobnosti.png


Kako usposabljanju dodamo kvalifikacijo?
++++++++++++++++++++++++++++++++++++++++

Izberemo podmeni Zaposleni ki se nahaja v modulu Kadrovanje in nato izberemo opcijo Planirana usposabljanja.
Kliknemo na gumb Dnevnik plana usposabljanja. Na vrhu strani izberemo zavihek Kvalifikacije.
V tem pogledu lahko dodajamo imena kvalifikacij, za katere se usposabljajo zaposleni. Kvalifikacijo dodamo
s pritiskom na gumb Dodaj kvalifikacijo. Obstoječo kvalifikacijo uredimo s klikom na ikono Uredi.
Kvalifikacijo izbrišemo s klikom na ikono Izbriši.

.. image:: https://gitlab.com/projekti/gedaxa-docs/raw/master/imgs/kvalifikacije.png


********************
Odsotnost zaposlenih
********************

Kako beležimo odsotnost zaposlenih?
+++++++++++++++++++++++++++++++++++

V podmeniju Zaposleni se nahaja opcija Odsotnost zaposlenih, kjer lahko beležimo, kdaj so bili zaposleni odsotni
od delovnega mesta. V polju Uporabniki izberete ime zaposlenega, za katerega želite vpisati odsotnost.
S klikom na Mesec izberete mesec, v katerem želite vpisati odsotnost. S klikom na gumb Potrdi, se osvežijo podatki
v spodnjih dveh tabelah.

.. image:: https://gitlab.com/projekti/gedaxa-docs/raw/master/imgs/odsotnost-zaposlenih.png

Prva tabela Dodeljene ure za izbrani mesec prikaže v stolpcu Pričakovano število ur na teden za koliko ur je
delojemalec pogodbeno zaposlen in v stolpcu Število ur v izbranem mesecu koliko ur ima že dodeljenih na izmenah v
izbranem mesecu.

Pod tabelo je koledarski mesec razdeljen na tedne. Za vsak dan v tednu lahko s klikom označite, če je odsotnost
zaposlenega zaradi Bolniške, Dopusta ali Neplačanega dopusta.

Če želite izbrati daljše obdobje odsotnosti, lahko v oknu Koliko dni hkrati vnesete število dni odsotnosti
in pritisnete Potrdi. Sedaj v koledarju izberete prvi dan odsotnosti in sistem avtomatično zabeleži
odsotnost za število dni, ki ste jih izbrali.

.. image:: https://gitlab.com/projekti/gedaxa-docs/raw/master/imgs/odsotnost-vec-dni.png

Možno je vpisati odsotnost za več zaposlenih istočasno. V polju Uporabniki izberete vse zaposlene, za katere
želite vpisati odsotnost. Po osvežiti koledarja se v prvem stolpcu pojavijo imena zaposlenih, za katere izberete
dneve odsotnosti. Odsotnost vsakega zaposlenega se beleži v svoji vrstici. Če ste izbrali opcijo Koliko dni hkrati se
vsakemu zaposlenemu avtomatično dodeli število odsotnih dni.
Z izbiro opcije Hkratna izbira omogočite, da se več zaposlenim zabeleži ista vrsta odsotnosti (npr. dopust) za
več dni (če ste izbrali to opcijo) z istim začetnim dnevom. Ta funkcionalnost poenostavi vpisovanje odsotnosti kot so
kolektivni dopusti ali turnusi izmen.

*********************
Prisotnost zaposlenih
*********************

Kako beležimo prisotnost zaposlenih?
++++++++++++++++++++++++++++++++++++

V podmeniju Zaposleni se nahaja opcija Prisotnost zaposlenih, kjer so zabeležene ure prisotnosti za posameznega
zaposlenega na določen dan. S klikom na gumb Dodaj prisotnost se odpre forma, kjer vnesemo ime zaposlenega iz
padajočega menija Zaposleni, Datum, na katerega se vnos nanaša, in Število opravljenih ur za izbran datum.
S klikom na gumb Potrdi se ure zabeležijo v tabeli Seznam prisotnosti.

.. image:: https://gitlab.com/projekti/gedaxa-docs/raw/master/imgs/dodaj-prisotnost.png

Zabeležene ure lahko uredite s klikom na ikono Uredi. Če želite pregledati prisotnost na določeni datum ali za
določenega zaposlenega (ali oboje) kliknete na Filtriraj tabelo in vpišete informacije po katerih želite filtrirati
Seznam prisotnosti.

.. |thick| raw:: html

    <i class="icon-check"></i>

.. |pencil| raw:: html

    <i class="icon-pencil2"></i>

.. |delete| raw:: html

    <i class="icon-close"></i>

.. |object| raw:: html

    <i class="icon-file-import"></i>

.. |icon_file| raw:: html

    <i class="icon-file"></i>

.. |icon_bill| raw:: html

   <i class="icon-bill-2"></i>

.. |icon_edit| raw:: html

   <i class="icon-edit-1"></i>

.. |icon_download| raw:: html

   <i class="icon-download-2"></i>

.. |icon_check_circle| raw:: html

   <i class="icon-check-circle"></i>

.. |icon_cancel_circle| raw:: html

   <i class="icon-cancel-circle"></i>

.. |icon_close| raw:: html

   <i class="icon-close"></i>

.. |icon_trash| raw:: html

   <i class="icon-trash"></i>

.. |icon_arrow_right| raw:: html

   <i class="icon-arrow-2-right"></i>

.. |icon_files| raw:: html

   <i class="icon-files"></i>

.. |icon_updown| raw:: html

   <i class="icon-swap-vertical"></i>
