.. gedaxa documentation master file, created by
   sphinx-quickstart on Wed Apr 26 11:23:43 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.


Gedaxa dokumentacija
====================

=============================
Kako mi dokumentacija pomaga?
=============================

Dokumentacija razloži kako deluje vsak del Gedaxa ERP. Lahko ga beremo na dva načina:

1. Po sklopih glavnega menija (kot si sledijo funkcionalnosti v sistemu), ali
2. Poiščemo zgolj odgovor na konkretno vprašanje.

Zgrajen je bil s podjetji iz CNC industrije, za CNC industrijo, da odgovarja na potrebe industrije. Zato je logika
sistema, da deluje kot projekt. Če vnašamo nabavo, dobavo, naročila, dodelimo delavne naloge… vse je vezano na projekte,
ki so v osrčju delovanja Gedaxa ERP.


*******************************
Kako začnem uporabljati Gedaxo?
*******************************

Vsak sistem je zgrajen iz gradnikov. Isto velja za Gedaxa ERP. Da bi sistem pravilno deloval moramo vnesti nekatere
informacije, ki jih sistem zveže med sabo:

1. Podatke o zaposlenih
2. Izmene in delovne vloge v podjetju
3. Šifrante materialov in strojev
4. Operacije, cikle in procese strojev
5. Skladišča in zaloge materiala
6. Partnerje: dobavitelje in naročnike

Brez skrbi, dokumentacija in sam sistem vas vodi skozi vnašanje potrebnih informacij.
Proces vnašanja je pa narejen na uporabniku prijazen način.


*******************
Kaj pomenijo ikone?
*******************

Vseskozi Gedaxo uporabljamo iste ikone, da lažje (tj. z manj branja in pisanja) uporabljamo Gedaxo. Vedno uporabljamo
iste ikone s sledečimi pomeni:

.. raw:: html

    <table>
        <tr>
            <th>Ikona</th>
            <th>Opis</th>
            <th>Ikona</th>
            <th>Opis</th>
        </tr>
        <tr>
            <td><i class="icon-swap-vertical"></i></td>
            <td>Filtriraj podatke v tabeli naraščajoče, padajoče</td>
            <td><i class="icon-check"></i></td>
            <td>Potrdi oziroma zaključi</td>
        </tr>
        <tr>
            <td><i class="icon-arrow-1-right"></i></td>
            <td>Poglej podrobnosti podatkov v vrstici</td>
            <td><i class="icon-close"></i></td>
            <td>Izbriši podatek</td>
        </tr>
        <tr>
            <td><i class="fa icon-calendar"></i></td>
            <td>Datum</td>
            <td><i class="icon-cancel-circle"></i></td>
            <td>Prekliči tekoči projekt</td>
        </tr>
        <tr>
            <td><i class="icon-pencil2"></i></td>
            <td>Uredi podatek / vpis</td>
            <td></td>
            <td>Preglej in uredi skupino / člane skupine</td>
        </tr>
        <tr>
            <td><i class="icon-user-male-options"></i></td>
            <td>Uredi pravice skupine</td>
            <td><i class="icon-graph"></i></td>
            <td>Statistika</td>
        </tr>
        <tr>
            <td><i class="icon-file"></i></td>
            <td>PDF dokument</td>
            <td><i class="icon-files"></i></td>
            <td>Datoteke</td>
        </tr>
        <tr>
            <td><i class="fa icon-calendar"></td>
            <td>Časovno obdobje, trajanje</td>
            <td><i class="icon-bill-2"></i></td>
            <td>Ustvari obračun potnega naloga</td>
        </tr>
        <tr>
            <td><i class="icon-clipboard-checked"></i></td>
            <td>Izdan račun</td>
            <td><i class="icon-papyrus"></i></td>
            <td>Ustvari mesečno poročilo</td>
        </tr>
    </table>

Če miško postavite nad ikono, se prikaže opis ikone in njene funkcije, da v Gedaxi čim hitreje najdete, kar iščete.

Dokumentacija za uporabo Gedaxa sistema
=======================================

Dokumentacija je organizirana v sklope kot si sledijo v navigacijskem meniju Gedaxe:

* :ref:`glavni-meni`
* :ref:`kadrovanje`
* :ref:`vodenje-projektov`
* :ref:`skladiscenje`
* :ref:`proizvodnja`
* :ref:`poslovanje`
* :ref:`nastavitve`


.. _glavni-meni:

.. toctree::
   :maxdepth: 2
   :caption: Glavni meni

   dashboard
   moj_urnik
   poslovna_analitika


.. _kadrovanje:

.. toctree::
   :maxdepth: 2
   :caption: Kadrovanje

   zaposleni
   izmene
   potni_nalogi
   organigram


.. _vodenje-projektov:

.. toctree::
   :maxdepth: 2
   :caption: Vodenje projektov

   seznam_projektov
   casovnice_projektov
   vse_ponudbe
   vsa_narocila
   vse_dobavnice


.. _skladiscenje:

.. toctree::
   :maxdepth: 2
   :caption: Skladiščenje

   material_in_skladisca
   inventura
   ostanki


.. _proizvodnja:

.. toctree::
   :maxdepth: 2
   :caption: Proizvodnja

   sifranti
   procesi
   stroji
   delovni_nalogi


.. _poslovanje:

.. toctree::
   :maxdepth: 2
   :caption: Poslovanje

   partnerji
   dobava
   racuni


.. _nastavitve:

.. toctree::
   :maxdepth: 2
   :caption: Nastavitve

   nastavitve


.. |thick| raw:: html

    <i class="icon-check"></i>

.. |pencil| raw:: html

    <i class="icon-pencil2"></i>

.. |delete| raw:: html

    <i class="icon-close"></i>

.. |object| raw:: html

    <i class="icon-file-import"></i>

.. |icon_file| raw:: html

    <i class="icon-file"></i>

.. |icon_bill| raw:: html

   <i class="icon-bill-2"></i>

.. |icon_edit| raw:: html

   <i class="icon-edit-1"></i>

.. |icon_download| raw:: html

   <i class="icon-download-2"></i>

.. |icon_check_circle| raw:: html

   <i class="icon-check-circle"></i>

.. |icon_cancel_circle| raw:: html

   <i class="icon-cancel-circle"></i>

.. |icon_close| raw:: html

   <i class="icon-close"></i>

.. |icon_trash| raw:: html

   <i class="icon-trash"></i>

.. |icon_arrow_right| raw:: html

   <i class="icon-arrow-2-right"></i>

.. |icon_files| raw:: html

   <i class="icon-files"></i>

.. |icon_updown| raw:: html

   <i class="icon-swap-vertical"></i>
