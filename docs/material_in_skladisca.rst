=====================
MATERIAL IN SKLADIŠČA
=====================

*Material in skladišča* nam ponuja pogled v katerem lahko urejamo material, ga preskladiščimo, pregledamo njegov
dnevnik, ter urejamo vsa naša skladišča.

********************
Kako uredim material
********************

Pred dodajanjem novega materiala, moramo poskrbeti, da se material nahaja v šifrantu (za pomoč poglejmo del
dokumentacije z naslovom *Kako dodamo nov šifrant*). Prav tako moramo imeti ustvarjeno skladišče v katerega želimo
dodati material (za pomoč poglejmo del dokumentacije z naslovom *Kako dodam novo skladišče?*).


Nov material dodamo tako, da v glavnem meniju izberemo *Material in skladišča* in nato v podmeniju možnost
*Uredi material*. Pokaže se nam tabela s seznamom vseh obstoječih materialov. Na dnu tabele izberimo možnost
*Uredi material*. S klikom na gumb *Uredi material* se nam odpre obrazec za vnos podatkov. Za *Dejanje* izberemo
eno izmed možnosti: *Dodaj na voljo* (Dodajanje materiala v skladišče), *Odstrani na voljo* (odstranjevanje materiala iz
skladišča), *Dodaj rezervacijo* (Dodajanje rezervacije na *Projekt*), *Odstrani rezervacijo* (odstranjevanje materiala
s *Projekta*, s tem tudi iz skladišča), *Spremeni rezeravcijo v na voljo* (odstranjevanje materiala s *Projekta* in
pri tem dodajanje v skladišče). S pomočjo iskalnega okna vnesemo material in skladišče.
Vnesemo še količino, ki jo želimo dodati, ter izbirno *Projekt* (v primerih, ko je ta potreben). Vnos materiala
zaključimo s klikom na gumb *Potrdi*.

.. image:: https://gitlab.com/projekti/gedaxa-docs/raw/master/imgs/material_1.png


***************************************************
Kaj pomeni stolpec Na voljo/Pričakovano/Rezervirano
***************************************************

Stolpec *Na voljo/Pričakovano/Rezervirano* nam prikazuje razmerje med materialom, ki ga lahko uporabljamo, materialom,
ki ga pričakujemo (naročilo) in materialom, ki je rezerviran za projekte. Material, ki je rezerviran je vedno
podmnožica materiala, ki je na voljo.

.. image:: https://gitlab.com/projekti/gedaxa-docs/raw/master/imgs/material_reserved.png

****************************
Kako preskladiščimo material
****************************

Pogoj: preskladiščimo lahko samo material, ki je že v skladišču.

Če želimo material preskladiščiti iz trenutnega skladišča v skladišče, ki še ne obstaja, je potrebno novo skladišče
predhodno ustvariti (za pomoč poglejmo del dokumentacije z naslovom *Kako dodamo novo skladišče*).

Material lahko preskladiščimo na dva načina:

1. Preko grafičnega vmesnika
2. S pomočjo obrazca Uredi material

Preko grafičnega vmesnika preskladiščimo material tako, da v glavnem meniju izberemo *Material in skladišča*.
Pokaže se nam tabela s seznamom vseh materialov. Na dnu tabele s klikom na gumb *Preskladišči material* se nam odpre
obrazec za vnos podatkov.

.. image:: https://gitlab.com/projekti/gedaxa-docs/raw/master/imgs/material_transfer.png

Izbrati moramo material iz šifranta in količino, ki predstavlja eno enoto materiala, katerega želimo prenesti. S klikom
na gumb *Potrdi* se nam odpre grafični vmesnik za preskladiščenje materiala. Material lahko z akcijo povleci in spusti
(angl. drag&drop) poljubno premikamo med skladišči. Spremembe se shranijo samodejno.

Grafični vmesnik prikaže material za preskladiščenje v kockah, ki predstavljajo količino, ki smo jo določili v
obrazcu zgoraj. Kocke dajejo vpogled nad tem, kolikšen delež celotnega materiala želimo preskladiščiti. Zelene kocke
je možno preskladiščiti, medtem ko sivih ni, saj te predstaljajo material, ki jerezerviran za določen projekt.

S pomočjo obrazcca preskladiščimo material tako, da v glavnem meniju izberemo *Urejanje materiala* in nato v.
Pokaže se nam tabela s seznamom vseh materialov. Na dnu tabele s klikom na gumb *Uredi material* se nam odpre obrazec
za vnos podatkov. Za preskladiščenje moramo izbrati dejanje *Odstrani* (pri katerem odstranjujemo material iz skladišča)
in nato dejanje *Dodaj* (v katero skladišče želimo preskladiščiti).

Omejitve: preskladiščimo lahko samo en material hkrati. Grafični mesnik za preskladiščenje materiala ne moremo
uporabljati na zaslonih na dotik.

.. image:: https://gitlab.com/projekti/gedaxa-docs/raw/master/imgs/material_transger_gui.png

*********************************************************
Kako preverimo kaj se je s posameznim materialom dogajalo
*********************************************************

Kdo je dodal ali odstranil material, kdo ga je rezerviral, kam se je material premikal in drugo preverimo tako, da
v glavnem meniju izberemo *Material in skladišča*. Pokaže se nam tabela s seznamom vseh materialov. S klikom na gumb
*Poglej dnevnik* se nam odpre tabela, ki vsebuje podatke o tem kdo, koliko in kateri material se je iz posameznega
skladišča premikal ter kdaj se je to zgodilo.

.. image:: https://gitlab.com/projekti/gedaxa-docs/raw/master/imgs/material_log_1.png


.. image:: https://gitlab.com/projekti/gedaxa-docs/raw/master/imgs/material_log_2.png


**************************
Kako dodamo novo skladišče
**************************

Novo skladišče dodamo tako, da v glavnem meniju izberemo *Material in skladišča* in nato v podmeniju možnost
*Uredi material*. Pokaže se nam tabela s seznamom vseh materialov. Na vrhu tabele izberimo možnost *Skladišče*.
S klikom na gumb *Dodaj skladišče* se nam odpre obrazec za vnos podatkov. *Vnesimo *Ime* (poljubno unikatno ime) in
opis. Urejanje zaključimo s klikom na gumb Potrdi.

.. image:: https://gitlab.com/projekti/gedaxa-docs/raw/master/imgs/material_warehouse_add.png

****************************************
Kaj je skladišče pričakovanega materiala
****************************************

Skladišče pričakovanega materiala je sistemsko skladišče, ki ga ni mogoče zbrisati. V njem se nahaja material, ki smo
ga naročili pri dobavitelju vendar zanj še nismo vnesli dobavnice. Z zaključevanjem dobavnice materiala se material
preskladiši v izbrana skladišča (zaključevanje dobavnice se nahaja v sekciji *Dobavnice*).
S klikom na gumb > poleg imena skladišča se nam prikaže zaloga materiala v skladišču.

*****************************
Kaj je Nerazvrščeno skladišče
*****************************

Nerazvrščeno skladišče je sistemsko skladišče, ki ga ni mogoče zbrisati. V njem se nahajajo  produkti, ki smo jih
naredili v naših procesih in jih skladiščimo po končanem proizvodnjem procesu. S klikom na gumb > poleg imena skladišča
se prikaže zaloga materiala v skladišču.

.. image:: https://gitlab.com/projekti/gedaxa-docs/raw/master/imgs/material_warehouse_system.png
