=============
VSE DOBAVNICE
=============

************************
Kako ustvarim dobavnico?
************************

Dobavnico za naročilo ustvarite v podmeniju **Vse dobavnice** (*Vodenje projektov > Vse dobavnice*).
Pokaže se vam tabela s seznamom vseh dobavnic.

.. image:: https://gitlab.com/projekti/gedaxa-docs/raw/master/imgs/dobavnice/list.png

S klikom na gumb *Ustvari dobavnico* se vam bo odprla forma za vnos podatkov.
Izberemo stranko in pa unikatno naključno šifro.
S klikom na gumb *Potrdi* se nam pokaže dobavnica za izbrano stranko.

.. image:: https://gitlab.com/projekti/gedaxa-docs/raw/master/imgs/dobavnice/create.png

Pri dobavnici vidimo vsa odprta naročila, ki jih imamo za to stranko.
Če želimo zapreti določeno postavko prvo kliknemo na material (puščica levo od imena materiala) v naročilu.
To nam odpre formo, ki omogoča, da se odločimo iz katerega skladišča in v kateri enoti bomo material prenesli na dobavnico.
Vnesemo količino.
Stanje shranimo s klikom na gumb *Shrani* in zaključimo s klikom na gumb *Zaključi*.
Ko dobavnico zaključimo, je ne moremo več urejati.

******************************************************
Kako dodam storitev na dobavnico ali uredim obstoječo?
******************************************************

V podmeniju *Vse dobavnice* (*Vodenje projektov > Vse dobavnice*), kliknemo na ikono za urejanje (|pencil|) v vrstici izbrane dobavnice.
Odpre se novo okno z naslovom *Dobavnica - šifra dobavnice*.
S klikom na gumb Dodaj storitev se odpre vnosna forma, kjer izberemo storitev iz baze šifrantov.
Če šifranta storitve, ki jo želimo dodati, ni na voljo, moramo šifrant šele vnesti.
Glej poglavje *Kako dodam nov šifrant?*.

Ko formo izpolnemo, kliknemo gumb **Dodaj**, in storitev se zabeleži v tabeli na strani.

.. image:: https://gitlab.com/projekti/gedaxa-docs/raw/master/imgs/dobavnice/storitev.png

Storitev lahko uredimo pred pošiljanjem dobavnice s klikom na gumb |pencil| ali zbrišemo iz dobavnice s klikom na |icon_trash|.

.. image:: https://gitlab.com/projekti/gedaxa-docs/raw/master/imgs/dobavnice/storitev_uredi.png

*********************************
Kako dodam datoteke na dobavnico?
*********************************

V podmeniju *Vse dobavnice* (*Vodenje projektov > Vse dobavnice*), kliknem na ikono za datoteke |icon_files| v vrstici
dobavnice, na katero želim dodati datoteke. Odpre se novo okno Datoteke dobavnice. Datoteko dodamo v dveh korakih.
kliknemo gumb *Dodaj datoteko*, nato pa v formi za vnos naložimo dadoteko iz računalnika, izberemo opis datoteke in
kliknemo gumb *Shrani*.

Naložena datoteka se bo prikazala v tabeli znotraj okna *Datoteke dobavnice*.
Lahko jo na izbrišemo |icon_close| ali prenesemo |icon_download| s klikom na izbrano ikono v stolpcu *Ukazi*.

************************
Kako izbrišem dobavnico?
************************

V podmeniju *Vse dobavnice* (Vodenje projektov > Vse dobavnice*) kliknem na ikono |icon_close| v vrstici dobavnice,
ki jo želimo izbrisati.

Odpre se nam okno, ki nas za vpraša, da potrdimo izbris dobavnice.
Ko kliknemo gumb *Potrdi*, smo izbrisali dobavnico iz sistema.


.. |thick| raw:: html

    <i class="icon-check"></i>

.. |pencil| raw:: html

    <i class="icon-pencil2"></i>

.. |delete| raw:: html

    <i class="icon-close"></i>

.. |object| raw:: html

    <i class="icon-file-import"></i>

.. |icon_file| raw:: html

    <i class="icon-file"></i>

.. |icon_bill| raw:: html

   <i class="icon-bill-2"></i>

.. |icon_edit| raw:: html

   <i class="icon-edit-1"></i>

.. |icon_download| raw:: html

   <i class="icon-download-2"></i>

.. |icon_check_circle| raw:: html

   <i class="icon-check-circle"></i>

.. |icon_cancel_circle| raw:: html

   <i class="icon-cancel-circle"></i>

.. |icon_close| raw:: html

   <i class="icon-close"></i>

.. |icon_trash| raw:: html

   <i class="icon-trash"></i>

.. |icon_arrow_right| raw:: html

   <i class="icon-arrow-2-right"></i>

.. |icon_files| raw:: html

   <i class="icon-files"></i>

.. |icon_updown| raw:: html

   <i class="icon-swap-vertical"></i>
