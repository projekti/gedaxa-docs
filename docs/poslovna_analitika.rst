==================
POSLOVNA ANALITIKA
==================

Poslovna analitika omogoča hitro in pregledno analitiko nad finančnim delovanjem podjetja.

*********************************
Kako uporabim poslovno analitiko?
*********************************

V formi takoj pod naslovom *Metrike vseh projektov* najprej določimo začetni in končni datum v koledarju. Vsi projekti (tj. naročila naročnikov), ki so se *začeli* znotraj izbranega obdobja, bodo upoštevani v analitiki. Nato izberemo, kateri tip podatkov želimo, da nam prikaže na spodnjih grafih. Na voljo imamo tri opcije:


1. *Predvidevane količine* - Prikaže predvidevane količine kot na začetki projekta lede na trenutno uporabo za vsak projekt ločeno.

2. *Dejanske količine* - Prikaže dejanske količine za vsak projekt ločeno. kot se je dejansko porabljalo (material dodatni, nadure)

3. *Združeni projekti* - Prikaže dejanske in predvidevane količine za vse projekte v izbranem časovnem obdobju.

S klikom na gumb *Potrdi* se analitika posodobi za iskano obdobje in tip podatkov.

.. image:: https://gitlab.com/projekti/gedaxa-docs/raw/master/imgs/poslovna_analitika1.png

Ko potrdimo izbor datumov in tipov podatkov se pod formo prikaže okno *Prikazujem za projekte*, kjer so našteta vsi projekti, ki jih vključuje analiza spodnjih grafov.

*********************************
Kako razumeti *Proračunski graf*?
*********************************
Vas zanima, kako profitabilni bo projekt, še preden se zaključi? Proračunski graf za izbrano obdobje prikaže razliko med pričakovanimi prihodki od projektov (iz ponudbe naročila) in stroški izvedbe projekta.

Stroški vključujejo človeško delo, strojno delo in amortizacijo, stroške materiala ter druge - ročno vnesene - stroške.

.. image:: https://gitlab.com/projekti/gedaxa-docs/raw/master/imgs/poslovna_analitika2.png

Z razliko med pričakovanimi prihodki ob zaključku projekta in odbitkom stroškov vidimo, kako stroški vplivajo na proračun projektov, kako hitro pada profitabilnost projektov in na kateri točki postane projekt neprofitabilen.

*********************************
Kako razumeti *Porabo materiala*?
*********************************

Tabela *Poraba materiala* prikaže za izbrane projekte, kateri *Material* je bil predviden v projektih (z imenom in šifro), njegovo *Predvideno količino* v različnih enotah (kg, m, l, kos) in kolikšna je bila *Dejanska količina* uporabljenega materiala.

.. image:: https://gitlab.com/projekti/gedaxa-docs/raw/master/imgs/poslovna_analitika3.png

***************************
Kako razumeti *Slabe kose*?
***************************

Tabela *Slabi kosi* prikaže ime in šifrant *Materiala*, ki je znotraj proizvodnje odpadel kot slab kos. Slabi kosi so razdelani po *Količini* (v različnih enotah) ter po *Razlogih* za slabe kose. Gedaxa črša razloge za slabe kose iz informacij v delovnem nalogu, kjer vpisujemo slabe kose proizvodnje.

.. image:: https://gitlab.com/projekti/gedaxa-docs/raw/master/imgs/poslovna_analitika4.png

*************************
Kako razumeti *Prihodek*?
*************************

Tabela *Prihodek* prikazuje skupni prihodek iz naročilnic za projekte iz izbranega obdobja. Če so se projekti le začeli in ne še končali v izbranem obdobju, nam tabel *Prihodek* prikaže vsoto, ki si jo lahko obetamo, ko bodo naročniki plačali naročilo.

.. image:: https://gitlab.com/projekti/gedaxa-docs/raw/master/imgs/poslovna_analitika5.png

***************************
Kako razumeti *Neplačnike*?
***************************

Tabela *Neplačniki* izpiše vse poslovne partnerje, ki so zamudili rok plačila. Gedaxa v tabeli izpiše samo naročnike, čigar roki plačila so znotraj izbranega obdobja. Če želimo pregledati vse neplačnike (oziroma odprte terjatve), preprosto izberemo daljše obdobje. Poleg imena neplačnika vidimo tudi odstotek neplačanih plačil.

.. image:: https://gitlab.com/projekti/gedaxa-docs/raw/master/imgs/poslovna_analitika6.png

***********************************
Kako razumeti *Kritične naročnike*?
***********************************

Tabela *Kritični naročniki* izpiše tiste poslovne partnerje, ki so lahko rizični za poslovanje podjetja. V njej najdemo naročnike, ki zamujajo ali *so v preteklosti* zamujali s plačilom. Torej, tudi če je naročnik že plačal račun, a ga je plačal z zamudo, je lahko uvrščen v tabelo *Kritični naročniki*. Tabela razvrsti kritične poslovne partnerje od tistih z največjimi povprečnimi zamudami do tistih z manjšimi zamudami.

.. image:: https://gitlab.com/projekti/gedaxa-docs/raw/master/imgs/poslovna_analitika7.png

*********************************
Kako razumeti *Največje stranke*?
*********************************

Tabela *Največje stranke* izpiše poslovne partnerje, ki so doprinesli podjetju največ prihodkov.

.. image:: https://gitlab.com/projekti/gedaxa-docs/raw/master/imgs/poslovna_analitika8.png

*************************************
Kako razumeti *Kritične dobavitelje*?
*************************************

Tabela *Kritični dobavitelji* izpiše poslovne partnerje, ki so zamudili z dostavo naročenega materiala in koliko dni so v povprečju zamudili.

.. image:: https://gitlab.com/projekti/gedaxa-docs/raw/master/imgs/poslovna_analitika9.png

*************************************
Kako razumeti *Največje dobavitelje*?
*************************************

Tabela *Največji dobavitelji* izpiše poslovne partnerje, ki so dostavili največjo količino materiala (*Celotna količina*) ter njihovo skupno število dobavnic.

.. image:: https://gitlab.com/projekti/gedaxa-docs/raw/master/imgs/poslovna_analitika10.png

.. |thick| raw:: html

    <i class="icon-check"></i>

.. |pencil| raw:: html

    <i class="icon-pencil2"></i>

.. |delete| raw:: html

    <i class="icon-close"></i>

.. |object| raw:: html

    <i class="icon-file-import"></i>

.. |icon_file| raw:: html

    <i class="icon-file"></i>

.. |icon_bill| raw:: html

   <i class="icon-bill-2"></i>

.. |icon_edit| raw:: html

   <i class="icon-edit-1"></i>

.. |icon_download| raw:: html

   <i class="icon-download-2"></i>

.. |icon_check_circle| raw:: html

   <i class="icon-check-circle"></i>

.. |icon_cancel_circle| raw:: html

   <i class="icon-cancel-circle"></i>

.. |icon_close| raw:: html

   <i class="icon-close"></i>

.. |icon_trash| raw:: html

   <i class="icon-trash"></i>

.. |icon_arrow_right| raw:: html

   <i class="icon-arrow-2-right"></i>
