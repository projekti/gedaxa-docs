==============
DELOVNI NALOGI
==============

***********************
SEZNAM DELOVNIH NALOGOV
***********************

Kako ustvarim nov delovni nalog?
********************************
**Pogoj**: Da bi lahko ustvarili delovni nalog, moramo imeti vpisano naročilo naročnika. Če nimamo, glej *Kako ustvarim novo naročilo*?

Ko želimo ustvariti delovni nalog, odpremo zavihek v meniju Seznam delovnih nalogov (*Glavni meni > Proizvodnja > Delovni nalogi > Seznam delovnih nalogov*). Prikaže se tabela s seznamom delovnih nalogov. Kliknemo na gumb **Dodaj delovni nalog**. Odpre se forma za vnos, kjer izberemo naročilo, za katero želimo ustvariti nov delovni nalog in potrdimo izbiro.

.. image:: https://gitlab.com/projekti/gedaxa-docs/raw/master/imgs/delovni_nalogi/ustvari_delovni_nalog_1.png

Uspešno ustvarjeni delovni nalog se zabeleži na dnu tabele *Seznam delovnih nalogov*. Avtomatično je status delovnega naloga označen z "Ni potrjeno" v rdečem okvirju, dokler ga ne potrditimo.

.. image:: https://gitlab.com/projekti/gedaxa-docs/raw/master/imgs/delovni_nalogi/ustvari_delovni_nalog_potrdilo.png

Kako potrdim delovni nalog?
***************************

V tabeli *Seznam delovnih nalogov* (*Glavni meni > Proizvodnja > Delovni nalogi > Seznam delovnih nalogov*) kliknemo na delovni nalog, ki ga želimo potrditi, v stolpcu *Naročilo*.

.. image:: https://gitlab.com/projekti/gedaxa-docs/raw/master/imgs/delovni_nalogi/potrdi_nalog.png

Odpre se novo okno *Pregled delovnega naloga*, kjer vidimo status delovnega naloga (potrjen, ne potrjen) na vrhu, nato podrobnosti delovnega naloga, nivojsko sestavnico, delne delovne naloge in zadolžitve vezane na delovni nalog. Preden je nalog potrjen, so vsa polja razen podrobnosti delovnega naloga prazna. S klikom na gumb Potrdi delovni nalog, tega potrdimo in status nad tabelo podrobnosti se spremeni v “Potrjeno” ter obarva z modro.

.. image:: https://gitlab.com/projekti/gedaxa-docs/raw/master/imgs/delovni_nalogi/potrdi_nalog2.png

Ko nalog potrdimo, se njegov status na vrhu strani spremeni v *Potrjeno*.

Kako ustvarim *delni* delovni nalog?
************************************

**Pogoj**: da bi ustvarili delni delovni nalog, moramo najprej imeti potrjen delovni nalog. Glej *Kako ustvarim delovni nalog?* in *Kako potrdim delovni nalog?* za več informacij.

V *Seznamu delovnih nalogov* (*Glavni meni > Proizvodnja > Delovni nalogi > Seznam delovnih nalogov*) kliknemo delovni nalog, ki ga želimo razdelati v delne delovne naloge.

.. image:: https://gitlab.com/projekti/gedaxa-docs/raw/master/imgs/delovni_nalogi/izberi_nalog.png

Odpre se stran *Pregled delovnega naloga*, kjer kliknemo na gumb *Dodaj delni delovni nalog*.

.. image:: https://gitlab.com/projekti/gedaxa-docs/raw/master/imgs/delovni_nalogi/dodaj_delni.png

Odpre se nam forma za vnos, kjer iz padajočega menija izberemo postavko naročila, proces, s katerim bomo izvedeli delni delovni nalog (če procesa ni med možnostmi, moramo dodati nov proces za proizvodnjo te postavke, glej Kako ustvarim nov proces?), rok delovnega naloga ter načrtovana količina v izbrani merski enoti za postavko naročila, ki jo proizvajamo. Ko smo zadovoljni z vnešenim potrdimo podatke s pritiskom na gumb.

.. image:: https://gitlab.com/projekti/gedaxa-docs/raw/master/imgs/delovni_nalogi/delni_forma.png

Ko uspešno ustvarimo delni delovni nalog se zgodi troje v sistemu:


1. Delni delovni nalog se vpiše v tabelo delnih delovnih nalogov. Lahko ga odstranimo ali spremenimo njegov rok izvedbe.

2. V tabeli *Postavke naročila* se izpiše kolikšen del celotnega naročila je bil doloćen za proizvodnjo skozi delni delovni nalog. Prav tako se naredi posebna nivojska sestavnica za vsak delni delovni nalog.

3. V tabeli *Podrobnosti naročila* se izpišejo datum začetka in predvidenega konca, ter skupni čas realizacije naročila skozi delovne naloge.

.. image:: https://gitlab.com/projekti/gedaxa-docs/raw/master/imgs/delovni_nalogi/delni_konec.png

Kako dodelim delavca na (delni) delovni nalog?
**********************************************

V *Pregledu delovnega naloga* (*Glavni meni > Proizvodnja > Delovni nalogi > Seznam delovnih nalogov > klik na izbran Delovni nalog*) kliknemo na *Uredi časovnico* za izbrani delni delovni nalog.

.. image:: https://gitlab.com/projekti/gedaxa-docs/raw/master/imgs/delovni_nalogi/uredi_casovnico.png

Odpre se *Razpored delovnega naloga*, kjer so koledarsko prikazani vsi delni delovni nalogi. Modro obarvani so že dodeljeni, oranžno obarvani pa niso bili še dodeljeni. S klik-povleci-spusti (angl. *drag and drop*) lahko (delni) delovni nalog premaknemo na drugo izmeno ali dan. S klikom na oranžni okvir, se pa odpre forma za vnos podatkov.

V formi za razporeditev delavca na delovni nalog je veliko polij predizponjenih, ker Gedaxa črpa iz baze podatkov šifrantov, procesov, strojev in zaposlenih. Izpolnimo manjkajoča polja in kliknemo *Potrdi*.

.. image:: https://gitlab.com/projekti/gedaxa-docs/raw/master/imgs/delovni_nalogi/forma_dodeli_delavca.png

Po uspešni dodelitvi (delnega) delovnega naloga delavcu se kvadrat obarva modro. Če nismo dodelili vseh postavk iz delovnega naloga, ko smo razporejali delavce, bo Gedaxa avtomatično razdelila (delni) delovni nalog na delni delovni nalog.

***************
MOJE ZADOLŽITVE
***************

Kako preverimo, kateri delovni nalogi so nam bili dodeljeni?
************************************************************

V opciji *Moje zadolžitve* (*Glavni meni > Proizvodnja > Delovni nalogi > Moje zadolžitve*) izberemo datum, ki nas zanima, in s klikom na gumb Potrdi se prikažejo vse naše zadolžitve na delovni nalog za izbrani datum.

.. image:: https://gitlab.com/projekti/gedaxa-docs/raw/master/imgs/delovni_nalogi/moje_zadolzitve.png

*******************
CELOVIT POGLED DELA
*******************

Kako preverim vse delovne naloge, ki so bili dodeljeni delavcem?
****************************************************************

V opciji *Celovit pogled dela* (*Glavni meni > Proizvodnja > Delovni nalogi > Celovit pogled dela*) izberemo obdobje (med dvema datuma), znotraj katerega želimo preveriti, kateri delovni nalogi so bili dodeljeni delavcem. Če želimo, lahko uporabimo dodatno funkcionalnost *Združi*, ki prikaže vse naloge vezan na določen projekt (*Združi > Projekt > izberemo poljuben projekt*) ali na določen stroj (*Združi > Stroj > izberemo poljuben stroj*).
S klikom na *Potrdi* se spodaj prikaže koledar po izmenah, kjer so vpisana opravila in delavci, ki so jim bila opravila dodeljena.

.. image:: https://gitlab.com/projekti/gedaxa-docs/raw/master/imgs/delovni_nalogi/pregled_nalog.png

Če postavimo kursor miške nad izbran nalog, se prikaže oblaček nad nalogom, ki na kratko predstavi podrobnosti delovnega naloga.

Kako preverim, kateri delovni nalogi še niso bili dodeljeni?
************************************************************

V opciji *Celovit pogled dela* (*Glavni meni > Proizvodnja > Delovni nalogi > Celovit pogled dela*) izberemo obdobje (med dvema datuma), znotraj katerega želimo preveriti, kateri delovni nalogi še niso bili dodeljeni delavcem. Vsa oranžno obarvana polja še niso bila dodeljena.

.. image:: https://gitlab.com/projekti/gedaxa-docs/raw/master/imgs/delovni_nalogi/pregled_nedodeljenih_nalog.png
