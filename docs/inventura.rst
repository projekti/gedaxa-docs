=========
INVENTURA
=========

Kako izdelam inventuro materiala (po skladiščih)?
*************************************************

V podmeniju *Inventura* (*Glavni meni > Skladiščenje > Inventura*) najdemo tabelo s preteklimi inventurami, osebo, ki jih je izdelala in datum izdelave inventure.
S pritiskom na gumb Izdelaj inventuro se odpre nov pogled, kjer je zabeležena količina materialov po skladiščih.

.. image:: https://gitlab.com/projekti/gedaxa-docs/raw/master/imgs/inventura/inventura_izdelaj.png

Nov pogled prikaže količino materiala (kot šifranta) po skladiščih. Če želimo lahko ročno spremenimo količine materiala v skladiščih s pritiskom na |pencil|. Lahko tudi dodamo nov material kot postavko s pritiskom na gumb *Dodaj postavko* na dnu strani.

.. image:: https://gitlab.com/projekti/gedaxa-docs/raw/master/imgs/inventura/inventura_izdelaj2.png

Ko smo zadovoljni z zalogami materiala po skladiščih, pritisnemo gumb *Zaključi & natisni* na dnu strani. Gedaxa ustvari novo okno s povzetkom inventure. Na vrhu okna najdemo gumb PDF, ki ga kliknemo, če želimo izvoziti inventuro v PDF datoteki (za tisk ali shranjevanje).

.. image:: https://gitlab.com/projekti/gedaxa-docs/raw/master/imgs/inventura/inventura_pdf.png

PDF preteklih inventur se avtomatično shranijo v arhiv sistema in so na voljo v podmeniju *Inventura* (*Glavni meni > Skladiščenje > Inventura*), v stolpcu *Ukazi*.


.. |thick| raw:: html

    <i class="icon-check"></i>

.. |pencil| raw:: html

    <i class="icon-pencil2"></i>
